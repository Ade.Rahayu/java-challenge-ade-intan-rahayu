package its.challenge.Ade_Intan_Rahayu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdeIntanRahayuApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdeIntanRahayuApplication.class, args);
	}

}
