package its.challenge.Ade_Intan_Rahayu.services;

import its.challenge.Ade_Intan_Rahayu.dto.ResultResponse;
import its.challenge.Ade_Intan_Rahayu.dto.UserRequest;

public interface AuthService {
    ResultResponse login(UserRequest request);
}
