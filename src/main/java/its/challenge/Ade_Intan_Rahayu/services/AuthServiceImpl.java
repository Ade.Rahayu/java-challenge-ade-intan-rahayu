package its.challenge.Ade_Intan_Rahayu.services;

import its.challenge.Ade_Intan_Rahayu.dto.ResultResponse;
import its.challenge.Ade_Intan_Rahayu.dto.UserRequest;
import its.challenge.Ade_Intan_Rahayu.models.User;
import its.challenge.Ade_Intan_Rahayu.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class AuthServiceImpl implements AuthService{
    @Autowired
    ResultResponse response;

    @Autowired
    UserRepository userRepository;

    public ResultResponse login(UserRequest request) {
        try {
            if (request.getUsername().isEmpty() || request.getPassword().isEmpty()) {
                response.setResult("username dan / atau password kosong");
                response.setHttpCode(HttpStatus.BAD_REQUEST.value());
                return response;
            }
            Optional<User> byUsernameAndPassword = userRepository.findByUsernameAndPassword(request.getUsername(), request.getPassword());
              if (byUsernameAndPassword.isEmpty()) {
                response.setResult("UNAUTHORIZED");
                response.setHttpCode(HttpStatus.UNAUTHORIZED.value());
                return response;
            }
            response.setResult("Sukses Login");
            response.setHttpCode(HttpStatus.OK.value());
            return response;
        } catch (Exception e) {
            response.setResult("INTERNAL SERVER ERROR");
            response.setHttpCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return response;
        }
    }
}
