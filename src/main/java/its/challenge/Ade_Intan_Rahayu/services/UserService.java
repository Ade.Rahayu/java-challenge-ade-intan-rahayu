package its.challenge.Ade_Intan_Rahayu.services;

import its.challenge.Ade_Intan_Rahayu.dto.ResultResponse;
import its.challenge.Ade_Intan_Rahayu.dto.UserRequest;

public interface UserService {
    ResultResponse insertUser(UserRequest userRequest);
    ResultResponse listUser();
    ResultResponse update(Long id,UserRequest request);


}
