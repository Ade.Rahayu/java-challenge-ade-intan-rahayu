package its.challenge.Ade_Intan_Rahayu.services;


import its.challenge.Ade_Intan_Rahayu.dto.ResultResponse;
import its.challenge.Ade_Intan_Rahayu.dto.UserRequest;
import its.challenge.Ade_Intan_Rahayu.models.User;
import its.challenge.Ade_Intan_Rahayu.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    ResultResponse response;

    @Override
    public ResultResponse insertUser(UserRequest userRequest) {
        try {
            boolean uNull = userRequest.getUsername() == null;
            boolean uEmpty = userRequest.getUsername().isEmpty();
            boolean pNull = userRequest.getPassword() == null;
            boolean pEmpty = userRequest.getPassword().isEmpty();

            if (uNull || uEmpty || pNull || pEmpty) {
                response.setHttpCode(HttpStatus.BAD_REQUEST.value());
                response.setResult("username or password can not be empty");
                return response;
            }
            Optional<User> byUsername = userRepository.findByUsername(userRequest.getUsername());
            if (byUsername.isPresent()) {
                response.setHttpCode(HttpStatus.CONFLICT.value());
                response.setResult("Username sudah terpakai");
            } else {
                userRepository.save(new User(userRequest.getUsername(), userRequest.getPassword()));
                response.setHttpCode(HttpStatus.CREATED.value());
                response.setResult("Success");
            }
            return response;
        } catch (Exception e) {
            return new ResultResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
    }

    @Override
    public ResultResponse listUser() {
        try {
            List<User> all = userRepository.findAll();
            response.setHttpCode(HttpStatus.OK.value());
            response.setResult(all);
            return response;
        } catch (Exception e) {
            return new ResultResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
    }

    @Override
    public ResultResponse update(Long id, UserRequest request) {
        try {
            boolean uNull = request.getUsername() == null;
            boolean uEmpty = request.getUsername().isEmpty();
            boolean pNull = request.getPassword() == null;
            boolean pEmpty = request.getPassword().isEmpty();

            if (uNull || uEmpty || pNull || pEmpty) {
                response.setHttpCode(HttpStatus.BAD_REQUEST.value());
                response.setResult("username or password can not be empty");
                return response;
            }
            Optional<User> byUsername = userRepository.findById(id);
            if (byUsername.isPresent()) {
                response.setHttpCode(HttpStatus.CONFLICT.value());
                response.setResult("Username sudah terpakai");
            }

            if (byUsername.get().getPassword().equals(request.getPassword())) {
                response.setHttpCode(HttpStatus.BAD_REQUEST.value());
                response.setResult("Password tidak boleh sama dengan sebelumnya");
                return response;
            }
            User user = byUsername.get();
            user.setUsername(request.getUsername());
            user.setPassword(request.getPassword());
            userRepository.save(user);

            response.setHttpCode(HttpStatus.CREATED.value());
            response.setResult("Success");
            return response;

        } catch (Exception e) {
            return new ResultResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
    }

}
