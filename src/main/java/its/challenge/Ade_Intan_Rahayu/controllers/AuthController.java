package its.challenge.Ade_Intan_Rahayu.controllers;

import its.challenge.Ade_Intan_Rahayu.dto.ResultResponse;
import its.challenge.Ade_Intan_Rahayu.dto.UserRequest;
import its.challenge.Ade_Intan_Rahayu.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    ResultResponse response;

    @Autowired
    AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<ResultResponse> login(@RequestBody  UserRequest request){
        try {
            ResultResponse resultResponse = authService.login(request);
            return new ResponseEntity<>(resultResponse, HttpStatus.valueOf(resultResponse.getHttpCode()));
        } catch (Exception e) {
            response.setResult("INTERNAL SERVER ERROR");
            return new ResponseEntity<>(response, HttpStatus.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
        }
    }
}
