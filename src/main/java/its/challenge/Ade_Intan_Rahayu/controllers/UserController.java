package its.challenge.Ade_Intan_Rahayu.controllers;


import its.challenge.Ade_Intan_Rahayu.dto.ResultResponse;
import its.challenge.Ade_Intan_Rahayu.dto.UserRequest;
import its.challenge.Ade_Intan_Rahayu.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/user")
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    ResultResponse response;

    @PostMapping("/create")
    public ResponseEntity<ResultResponse> insertUser(
            @RequestParam("username") String username,
            @RequestParam("password") String password) {
        try {
            UserRequest userRequest = UserRequest.builder()
                    .username(username)
                    .password(password).build();
            ResultResponse resultResponse = userService.insertUser(userRequest);
            return new ResponseEntity<>(resultResponse, HttpStatus.valueOf(resultResponse.getHttpCode()));
        } catch (Exception e) {
            response.setResult("INTERNAL SERVER ERROR");
            return new ResponseEntity<>(response, HttpStatus.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
        }
    }

    @GetMapping("/listAll")
    public ResponseEntity<ResultResponse> listAll() {
        try {
            ResultResponse resultResponse = userService.listUser();
            return new ResponseEntity<>(resultResponse, HttpStatus.valueOf(resultResponse.getHttpCode()));
        } catch (Exception e) {
            response.setResult("INTERNAL SERVER ERROR");
            return new ResponseEntity<>(response, HttpStatus.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ResultResponse> update(@PathVariable long id,@RequestBody UserRequest request) {
        try {
            ResultResponse resultResponse = userService.update(id, request);
            return new ResponseEntity<>(resultResponse, HttpStatus.valueOf(resultResponse.getHttpCode()));
        } catch (Exception e) {
            response.setResult("INTERNAL SERVER ERROR");
            return new ResponseEntity<>(response, HttpStatus.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
        }
    }
}

